import os

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])

class Config(object):
    CSRF_ENABLED = True
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'J5CoE2Aw85wKf6h'

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

