from app import application
from config import allowed_file
from app.forms import StudentRegistrationForm, SigninForm
from Database import Database_Facade as db
from flask import render_template, request, make_response
from werkzeug.utils import secure_filename
from functools import wraps
import json
import os

@application.errorhandler(404)
def page_not_found(e):
    return render_template('404.html', signin_form=SigninForm()), 404

@application.route('/', methods=['GET', 'POST'])
def landing_page():
    return render_template('/landing_page.html', page_title="Landing Page", signin_form=SigninForm())

@application.route('/signin', methods=['GET', 'POST'])
def signin():
    if request.method == 'POST':
        return render_template('signin.html', signin_form=SigninForm())
    return render_template('signin.html', signin_form=SigninForm())

@application.route('/signup', methods=['GET', 'POST'])
def signup():
    return render_template('signup.html', signin_form=SigninForm(), page_title='Register')

@application.route('/signup_student', methods=['GET', 'POST'])
def signup_student():
    if request.method == 'POST':
        pass
    return render_template('/signup_student.html', form=StudentRegistrationForm())


@application.route('/user', methods=['GET'])
def student_profile(pass_edit=False):

    user_id                 = request.args.get('id')
    edit                    = request.args.get('edit')

    # Fetch data from db
    natural_languages       = fetch_from_db(user_id, db.get_student_hlanguage)
    programming_languages   = fetch_from_db(user_id, db.get_student_planguage)
    other_skills            = fetch_from_db(user_id, db.get_student_otherskill)

    fullname                = db.get_student_name(user_id)
    city                    = db.get_student_city(user_id)
    university              = db.get_student_posname(user_id)
    major                   = db.get_student_major(user_id)
    quote                   = db.get_student_description(user_id)
    looking_for             = db.get_student_lookingfor(user_id)
    experience              = db.get_student_experience(user_id)

    profile_picture = db.get_student_picture(user_id)

    if edit == 'true' or edit == 'True':
        pass_edit = True
    elif edit == 'false' or edit == 'False':
        pass_edit = False

    return render_template('student_profile.html',
                           user_id=user_id,
                           page_title="Your Profile",
                           signin_form=SigninForm(),
                           fullname=fullname,
                           profile_picture=profile_picture,
                           city=city,
                           university=university,
                           major=major,
                           quote=quote,
                           looking_for=looking_for,
                           experience=experience,
                           natural_languages=natural_languages,
                           programming_languages=programming_languages,
                           other_skills=other_skills,
                           edit=pass_edit
                           )

def fetch_from_db(user_id, get):
    items = get(user_id)
    data = json.loads(items)
    return data

def update_db(user_id, data, update):
    data = json.dumps(data)
    update(user_id, data)

def handle_post(f):
    @wraps(f)
    def wrapper():
        user_id = request.args.get('id')
        data    = request.get_json()
        f(user_id, data)
        return ok_response()

    return wrapper

@application.route('/user/natural_languages', methods=['POST'])
@handle_post
def update_natural_languages(user_id, data):
    update_db(user_id, data, db.update_student_hlanguage)

@application.route('/user/programming_languages', methods=['POST'])
@handle_post
def update_programming_languages(user_id, data):
    update_db(user_id, data, db.update_student_planguage)

@application.route('/user/other_skills', methods=['POST'])
@handle_post
def update_other_skills(user_id, data):
   update_db(user_id, data, db.update_student_oskills)

@application.route('/user/fullname', methods=['POST'])
@handle_post
def user_update_fullname(user_id, data):
    update_db(user_id, data, db.update_student_name)

@application.route('/user/city', methods=['POST'])
@handle_post
def user_update_city(user_id, data):
    update_db(user_id, data, db.update_student_city)

@application.route('/user/major', methods=['POST'])
@handle_post
def user_update_major(user_id, data):
    update_db(user_id, data, db.update_student_major)

@application.route('/user/university', methods=['POST'])
@handle_post
def user_update_university(user_id, data):
    update_db(user_id, data, db.update_student_posname)

@application.route('/user/quote', methods=['POST'])
@handle_post
def user_update_quote(user_id, data):
    update_db(user_id, data, db.update_student_description)

@application.route('/user/looking_for', methods=['POST'])
@handle_post
def user_update_looking_for(user_id, data):
    update_db(user_id, data, db.update_student_lookingfor)

@application.route('/user/experience', methods=['POST'])
@handle_post
def user_update_experience(user_id, data):
    update_db(user_id, data, db.update_student_experience)

@application.route('/user/search', methods=['GET'])
def search_bar():
    query_string = request.query_string.decode('utf-8')
    raw_findings = db.search_students(query_string)
    findings = []

    for result in raw_findings:
        languages_json = json.loads(db.get_student_planguage(result.mail))
        languages = []
        for dic in languages_json:
            languages.append(dic['language'])

        finding = {
            'id': result.mail,
            'fullname': result.fullname,
            'city': result.city,
            'profile_picture': result.profile_picture,
            'languages': languages
        }
        findings.append(finding)

    return render_template('/search.html',
                           page_title="Search",
                           signin_form=SigninForm(),
                           findings=findings
                           )

@application.route('/upload', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        user_id = request.args.get('id')
        file = request.files['file']

        if file and allowed_file(file.filename):
            filename = user_id + "_" + secure_filename(file.filename)

            file.save(os.path.join(application.config['UPLOAD_FOLDER'], filename))

            img_path = "static/upload/" + filename
            db.update_student_picture(user_id, img_path)
            return img_path

def ok_response():
    resp = make_response()
    resp.headers['Content-Type'] = 'application/json'
    return resp

if __name__ == '__main__':
    application.run(debug=True)