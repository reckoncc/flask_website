from flask import Flask
from config import Config
import os
from flask_bootstrap import Bootstrap

# Application Setup
application = Flask(__name__, static_url_path='/static', static_folder="static")
application.config.from_object(Config)
Bootstrap(application)

UPLOAD_FOLDER = os.path.join(os.path.dirname(os.path.realpath(__file__)), "static/upload")
application.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

from app import routes