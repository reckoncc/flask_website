from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, DateField, SubmitField, validators

"""
.. module:: app.forms
    :synopsis: This class contains the Flask-WTF forms used for Signin and Student/Company Registration
"""

class SigninForm(FlaskForm):
    """Class for Signing In Form
    """
    email = StringField('Email', [validators.DataRequired(), validators.Email()])
    password = PasswordField('Password', [validators.Length(min=8, max=30)])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')

class StudentRegistrationForm(FlaskForm):
    """Class for Student Registration Form
    """
    fullname = StringField('Full Name', [validators.DataRequired()])
    email = StringField('Email', [validators.DataRequired(), validators.Email()])
    birthdate = DateField('Birthdate', [validators.DataRequired()])
    city = DateField('City', [validators.DataRequired()])
    university = StringField('University', [validators.DataRequired()])
    major = StringField('Major', [validators.DataRequired()])
    password = PasswordField('Password', [validators.Length(min=8, max=30)])
    password2 = PasswordField('Confirm Password', [validators.DataRequired(), validators.EqualTo('password')])
    submit = SubmitField('Sign Up')

class CompanyRegistrationForm(FlaskForm):
    """Class for Company Registration Form
    """
    company_name = StringField('Company Name', [validators.DataRequired()])
    contact_mail = StringField("Contact Mail", [validators.DataRequired()])

    def validate_email(self, email):
        """Validates the user's E-Mail using RegEx

        :param email: E-Mail enterer from user
        :return: True, if E-Mail valid; False, otherwise
        """
        import re
        if len(email) > 7:
            if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", email) != None:
                return True
        return False


    def validate_birthdate(self, birthdate):
        """Validates the user's birthdate

        :param birthdate: user's birthdate
        :return: true, if birthdate fits format and is logically coherent
        """

        import datetime
        try:
            year, month, day = birthdate.split("-")
            date = datetime.datetime(int(year),int(month),int(day))
            if date < datetime.datetime.now():
                return True
            else:
                print("You are not born yet!!")
            return False
        except ValueError:
            print("Incorrect data format,should be YYYY-MM-DD or invalid date")
            return False