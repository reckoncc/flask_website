# -*- coding: utf-8 -*-
"""
Created on Sun Apr 22 20:08:56 2018

@author: Nils Birkner

"""

from pony.orm import *
from datetime import date

db = Database()


# Now we enter the Databaseclasses
# These are only templates to create the given Database
# and will run only one time. Everytime after they get called
# , they ignore all templates.
class Student(db.Entity):
    mail = PrimaryKey(str)
    fullname = Required(str)
    birthdate = Required(date)
    city = Required(str)
    university = Optional(str)
    major = Optional(str)
    quote = Optional(str)
    looking_for = Optional(str)
    profile_picture = Optional(str)
    experience = Optional(str)
    git_account = Optional(str)


class Company(db.Entity):
    mail = PrimaryKey(str)
    name = Required(str)
    accept_appliances = Required(bool)
    contact_person_mail = Optional(str)
    contact_person = Optional(str)
    description = Optional(str)
    looking_for = Optional(str)
    profile_picture = Optional(str)


class Authentification(db.Entity):
    Auth_User_Mail = PrimaryKey(str)
    Auth_Password = Required(str)
    Auth_Is_Authentificated = Optional(bool)
    Auth_Is_Active = Optional(bool)
    Auth_Is_Anonymous = Optional(bool)


class Programming_Language(db.Entity):
    mail = Required(str)
    languages = Required(Json)


class Human_Language(db.Entity):
    mail = Required(str)
    languages = Required(Json)


class Other_Skills(db.Entity):
    mail = Required(str)
    skills = Required(Json)


# Now we need to bind the database to our program. This
# We also create the Database. If it is already created,
# It gets ignored.
db.bind(provider='sqlite', filename='maindb.db', create_db=True)
db.generate_mapping(create_tables=True)

if __name__ == 'main':
    pass