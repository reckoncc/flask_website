# -*- coding: utf-8 -*-
"""
Created on Fri Apr 27 19:24:21 2018

@author: Nils Birkner
The file "Database_MainFunctions.py" is the interface between the database
and the facade, which simplifies usage of the here existing functions.

The function are structured as follows:
    -> First we have the functional Functions, which are used for redundant
    tasks, like connection to the database.
    -> Second, we have the create-functions. These serve the purpose to create
    new instances of database-entries. They are also reliable to block ANY
    test of SQL-Injection.
    -> Third, we have the specialized change-functions for JSON-operations.
    -> Fourth, we have all get-functions to ensure, that every information
    is readable for the softwaresystem in place.

An important note: Because of the way "Pony" works, the create-functions
aside from Create_New_Account and Create_New_Authentification are
NOT required, due to the fact that creating a new data and updating data
is equal. Just for JSON-operations are special update-functions in place.
"""

# Import Standard-Modules first
from datetime import datetime
import json
import re

# Third-Party-Libs
import pony.orm

# Self-Created Libs
from Database import Database_Definition as DD

# Self-build exceptions to catch different possible problems
from Database import AccountNotFoundException as EX

""" Internal Functions """


# Connects to the database if a connection is not established
def dd_connect():
    db = pony.orm.Database()
    # Connect to the specified database, defined in Database_Definition.py
    db.bind(provider='sqlite', filename='maindb.db')


# Convert a date to a SQL-Date
def conv_to_date(date_input):
    # Date-Input: Year Month Day
    date_return = datetime.strptime(date_input, '%Y %m %d')
    return date_return


# Simple check if a mailaddress is a valid address
# param: mail = String
def check_mail_address(mail):
    security_counter = 0
    # If we have an @ everything should be fine,
    # if we have a ; we need to block this immediatly
    for iterator in mail:
        if iterator == '@':
            security_counter += 1
        if iterator == ';':
            raise EX.InvalidMailException
    if security_counter >= 1:
        return True
    else:
        raise EX.InvalidMailException


# Check if another account was already created with the given mail.
def check_duplicate_account(user_type, mail):
    if user_type == 'student':
        # If we get an object, the account is already used.
        duplicate = get_student_account(mail)
        if duplicate != None:
            raise EX.DuplicateAccountException
        else:
            return True
    else:
        duplicate = get_company_account(mail)
        if duplicate != None:
            raise EX.DuplicateAccountException
        else:
            return True


# Check if a language was already added
def check_json_duplicate(json_file, added_file):  # json_file = List, added_file = JSON-String
    added_dict = json.loads(added_file)
    for it in json_file:  # Iterate through the List
        key = json.loads(it)  # Load the actual Dictionary
        for val in key:  # For every Value in the Dictionary
            if val == "language":
                if key[val] == added_dict[val]:  # If our Language-Key is equal, Error
                    return False
    return True


""" This section represents the first part of our function-block. """


def make_new_student_account(mail, hash, name, birthday,
                             city="-", major="-", university="-", quote="-",
                             looking_for="-", profile_picture=None, experience="-",
                             git_account="-"):
    dd_connect()
    check_mail_address(mail)
    check_duplicate_account('student', mail)

    # If we have an initial picture, we need to convert it to bytes
    if profile_picture != None:
        pic_input = profile_picture
    else:
        pic_input = None
    date_input = conv_to_date(birthday)
    acc = DD.Student(mail=mail, fullname=name,
                     birthdate=date_input,
                     city=city, university=university,
                     major=major, quote=quote,
                     looking_for=looking_for,
                     profile_picture=profile_picture,
                     experience=experience, git_account=git_account)
    pony.orm.commit()
    make_new_authentification(acc.mail, hash)
    make_new_language(mail)


# End of Function

def make_new_company_account(mail, hash, name="-", accept_appliances=False,
                             contact_person="-", contact_person_mail="-",
                             description="-", looking_for="-", profile_picture=""):
    dd_connect()
    check_mail_address(mail)
    check_duplicate_account('company', mail)

    # If we have an initial picture, we need to convert it to bytes
    if profile_picture != "":
        pic_input = profile_picture
    else:
        pic_input = ""
    acc = DD.Company(mail=mail, name=name,
                     accept_appliances=accept_appliances,
                     contact_person_mail=contact_person_mail,
                     contact_person=contact_person,
                     description=description, looking_for=looking_for,
                     profile_picture=pic_input)
    pony.orm.commit()
    make_new_authentification(acc.mail, hash)


# End of Function


def make_new_authentification(mail, hash):
    auth = DD.Authentification(Auth_User_Mail=mail, Auth_Password=hash,
                               Auth_Is_Authentificated=True, Auth_Is_Active=True,
                               Auth_Is_Anonymous=False)
    if auth == None:
        raise EX.PasswordCreationError
    pony.orm.commit()


def make_new_language(mail):
    dd_connect()
    new_empty_list = []
    DD.Human_Language(mail=mail, languages=new_empty_list)
    DD.Programming_Language(mail=mail, languages=new_empty_list)
    DD.Other_Skills(mail=mail, skills=new_empty_list)
    pony.orm.commit()


def update_student_name(mail, data):
    dd_connect()
    Selection = get_student_account(mail)
    Selection.fullname = data
    pony.orm.commit()


def update_company_name(mail, data):
    dd_connect()
    Selection = get_company_account(mail)
    Selection.name = data
    pony.orm.commit()


def update_description(user_type, mail, data):
    dd_connect()
    if user_type == 'student':
        Selection = get_student_account(mail)
        Selection.quote = data
    else:
        Selection = get_company_account(mail)
        Selection.description = data

    pony.orm.commit()


def update_picture(user_type, mail, data):
    dd_connect()
    if data == None:
        if user_type == 'student':
            Selection = get_student_account(mail)
            Selection.profile_picture = ""
        else:
            Selection = get_company_account(mail)
            Selection.profile_picture = ""
    else:
        if user_type == 'student':
            Selection = get_student_account(mail)
            Selection.profile_picture = data
        else:
            Selection = get_company_account(mail)
            Selection.Company_ProfilePicutre = data

    pony.orm.commit()


def update_lookingfor(user_type, mail, data):
    dd_connect()
    if user_type == 'student':
        Selection = get_student_account(mail)
        Selection.looking_for = data
    else:
        Selection = get_company_account(mail)
        Selection.looking_for = data
    pony.orm.commit()


# Student
def update_city(mail, data):
    dd_connect()
    Selection = get_student_account(mail)
    Selection.city = data
    pony.orm.commit()


def update_major(mail, data):
    dd_connect()
    Selection = get_student_account(mail)
    Selection.major = data
    pony.orm.commit()


def update_experience(mail, data):
    dd_connect()
    Selection = get_student_account(mail)
    Selection.experience = data
    pony.orm.commit()


def update_github(mail, data):
    dd_connect()
    Selection = get_student_account(mail)
    Selection.Student_Github = data


def update_posname(mail, data):
    dd_connect()
    Selection = get_student_account(mail)
    Selection.university = data


# Company
def make_new_contact_person(mail, name="-", CMail="-"):
    dd_connect()
    Selection = get_company_account(mail)
    Selection.Company_CName = name
    Selection.Company_CMail = CMail
    pony.orm.commit()


# Update Function
def update_human_languages(mail, data):
    dd_connect()
    Selection = get_student_hlanguages(mail)
    Selection.languages = data
    pony.orm.commit()


def update_programming_languages(mail, data):
    dd_connect()
    Selection = get_student_planguages(mail)
    Selection.languages = data
    pony.orm.commit()


def update_other_skills(mail, data):
    dd_connect()
    Selection = get_student_oskills(mail)
    Selection.skills = data
    pony.orm.commit()


# Getter
def get_student_account(mail):
    dd_connect()
    Selection = DD.Student.get(mail=mail)
    return Selection


def get_company_account(mail):
    dd_connect()
    Selection = DD.Company.get(mail=mail)
    return Selection


def get_user_auth(mail):
    dd_connect()
    Selection = DD.Authentification.get(Auth_User_Mail=mail)
    return Selection


def get_student_hlanguages(mail):
    dd_connect()
    Selection = DD.Human_Language.get(mail=mail)
    return Selection


def get_student_planguages(mail):
    dd_connect()
    Selection = DD.Programming_Language.get(mail=mail)
    return Selection


def get_student_oskills(mail):
    dd_connect()
    Selection = DD.Other_Skills.get(mail=mail)
    return Selection


def get_student_picturepath(mail):
    dd_connect()
    Selection = DD.Student.get(mail=mail)
    return Selection.profile_picture


def get_company_picturepath(mail):
    dd_connect()
    Selection = DD.Company.get(mail=mail)
    return Selection.profile_picture


# Delete Functions
def delete_student_account(mail):
    dd_connect()
    Selection = get_student_account(mail)
    try:
        Selection_Auth = DD.Authentification.get(Auth_User_Mail=mail)
        Selection_Auth.delete()
    except:
        pass  # Nothing to delete here
    try:
        Selection_HL = get_student_hlanguages(mail)
        Selection_HL.delete()
    except:
        pass  # Nothing to delete here
    try:
        Selection_PL = get_student_planguages(mail)
        Selection_PL.delete()
    except:
        pass  # Nothing to delete here
    try:
        Selection_OS = get_student_oskills(mail)
        Selection_OS.delete()
    except:
        pass  # Nothing to delete here
    Selection.delete()
    pony.orm.commit()


def delete_company_account(mail):
    dd_connect()
    Selection = get_company_account(mail)
    try:
        Selection_Auth = DD.Authentification.get(Auth_User_Mail=mail)
        Selection_Auth.delete()
    except:
        pass  # Nothing to delete here

    Selection.delete()
    pony.orm.commit()


# Auth-Functions
def set_user_active(mail):
    dd_connect()
    with pony.orm.db_session:
        Selection = get_user_auth(mail)
        Selection.Auth_Is_Active = True


def set_user_authentificated(mail):
    dd_connect()
    Selection = get_user_auth(mail)
    Selection.Auth_Is_Authentificated = True


def set_user_inactive(mail):
    dd_connect()
    Selection = get_user_auth(mail)
    Selection.Auth_Is_Active = False


def set_user_unauthentificated(mail):
    dd_connect()
    Selection = get_user_auth(mail)
    Selection.Auth_Is_Active = False


def set_user_anonymous(mail):
    dd_connect()
    Selection = get_user_auth(mail)
    Selection.Auth_Is_Anonymous = True


def set_user_unanonymous(mail):
    dd_connect()
    Selection = get_user_auth(mail)
    Selection.Auth_Is_Anonymous = False


def get_user_active(mail):
    dd_connect()
    Selection = get_user_auth(mail)
    return Selection.Auth_Is_Active


def get_user_authentificated(mail):
    dd_connect()
    Selection = get_user_auth(mail)
    return Selection.Auth_Is_Authentificated


def get_user_anonymous(mail):
    dd_connect()
    Selection = get_user_auth(mail)
    return Selection.Auth_Is_Anonymous

def search_mail(searchlist):
    db_entry            = DD.Student.select
    db_selector         = lambda c: c.mail != None

    def filter(student, searchlist4comp):
        if student.mail.upper() in searchlist4comp:
            return True

    return search_for_attribute(searchlist, db_entry, db_selector, filter)

def search_name(searchlist):
    db_entry            = DD.Student.select
    db_selector         = lambda c: c.fullname != ""

    def filter(student, searchlist4comp):
        name_splitted = student.fullname.split(" ")
        for name_part in name_splitted:
            if name_part.upper() in searchlist4comp:
                return True

    return search_for_attribute(searchlist, db_entry, db_selector, filter)

def search_city(searchlist):
    db_entry            = DD.Student.select
    db_selector         = lambda c: c.fullname != ""

    def filter(student, searchlist4comp):
        city_splitted = re.split('[- ]', student.city)
        for city_part in city_splitted:
            if city_part.upper() in searchlist4comp:
                return True

    return search_for_attribute(searchlist, db_entry, db_selector, filter)

def search_university(searchlist):
    db_entry            = DD.Student.select
    db_selector         = lambda c: c.university != ""

    def filter(student, searchlist4comp):
        university_splitted = student.university.split(" ")
        for university_part in university_splitted:
            if university_part.upper() in searchlist4comp:
                return True

    return search_for_attribute(searchlist, db_entry, db_selector, filter, searchlist_filter={'HS', 'HTW', 'University', 'Institute', 'Institution', 'of'})

def search_major(searchlist):
    db_entry            = DD.Student.select
    db_selector         = lambda c: c.major != ""

    def filter(student, searchlist4comp):
        major_splitted = student.major.split(" ")
        for major_part in major_splitted:
            if major_part.upper() in searchlist4comp:
                return True

    return search_for_attribute(searchlist, db_entry, db_selector, filter)

def search_looking_for(searchlist):
    db_entry            = DD.Student.select
    db_selector         = lambda c: c.looking_for != ""

    def filter(student, searchlist4comp):
        looking_for_splitted = re.split("[.,:\-/ ]", student.looking_for)
        for looking_for_part in looking_for_splitted:
            if looking_for_part.upper() in searchlist4comp:
                return True

    return search_for_attribute(searchlist, db_entry, db_selector, filter)

def search_programming_languages(searchlist):
    db_entry            = DD.Programming_Language.select
    db_selector         = lambda c: len(c.languages) >= 0
    adder               = lambda x: get_student_account(x.mail)

    def filter(student, searchlist4comp):
        json_list = json.loads(student.languages)
        for dic in json_list:
            if dic["language"].upper() in searchlist4comp:
                return True

    return search_for_attribute(searchlist, db_entry, db_selector, filter, adder)

def search_natural_languages(searchlist):
    db_entry            = DD.Human_Language.select
    db_selector         = lambda c: len(c.languages) >= 0
    adder               = lambda x: get_student_account(x.mail)

    def filter(student, searchlist4comp):
        json_list = json.loads(student.languages)
        for dic in json_list:
            if dic["language"].upper() in searchlist4comp:
                return True

    return search_for_attribute(searchlist, db_entry, db_selector, filter, adder)

def search_other_skills(searchlist):
    db_entry            = DD.Other_Skills.select
    db_selector         = lambda c: len(c.skills) >= 0
    adder               = lambda x: get_student_account(x.mail)

    def filter(student, searchlist4comp):
        json_list = json.loads(student.skills)
        for dic in json_list:
            skill_splitted = dic["skill"].split(" ")
            for skill_part in skill_splitted:
                if skill_part.upper() in searchlist4comp:
                    return True

    return search_for_attribute(searchlist, db_entry, db_selector, filter, adder)

def search_for_attribute(searchlist, db_entry, db_selector, filter, adder=lambda c: c, delim="%20", searchlist_filter={}):
    dd_connect()
    searchlist4comp = { s.upper() for s in searchlist.split(delim) if s not in searchlist_filter }
    results = set()
    students = []
    for x in db_entry(db_selector):
        students.append(x)
    for student in students:
        if filter(student, searchlist4comp):
            results.add(adder(student))
    return results

def search_all(searchlist, searchfunctions={search_mail, search_name, search_city, search_university, search_major, search_looking_for, search_programming_languages, search_natural_languages, search_other_skills}):
    dd_connect()
    results = set()
    for searchfunction in searchfunctions:
        results |= searchfunction(searchlist)
    return results

if __name__ == 'main':
    pass