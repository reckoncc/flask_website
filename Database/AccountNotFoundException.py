# -*- coding: utf-8 -*-
"""
Created on Wed May  2 18:03:54 2018

@author: Stard
"""

class DuplicateAccountException(Exception):
    def __init__(self):
        pass
    
class InvalidMailException(Exception):
    def __init__(self):
        pass
    
class PasswordCreationError(Exception):
    def __init__(self):
        pass
    
class DuplicateLanguageException(Exception):
    def __init__(self):
        pass
    
if __name__ == 'main':
    pass