# -*- coding: utf-8 -*-
"""
Created on Fri Apr 27 19:29:27 2018

@author: Nils Birkner
This File represents the Database-Facade, which provides
every Interface which is/will be needed for accessing the
database.
It can easy maintained, as every function called here will
work in the background, so other developers don't need any

TODO: JSON -> Funktion, die Sprachen(alle) Parameter Ã¼bernimmt und die
korrespondierende JSON-File bearbeitet! -> Wenn eine Sprache schon vorhanden
ist, exception werfen!
detail.
"""
from pony.orm import db_session
from Database import Database_MainFunctions as DMF
from Database import AccountNotFoundException as EX


@db_session()
def create_student_account(mail, Psw_Hash, name="-", Geb="1900-01-01",
                           City="-", Major="-", POS="-", Desc="-",
                           LookingFor="-", Pic="", Experience="-",
                           Git="-"):
    try:
        DMF.make_new_student_account(mail, Psw_Hash, name, Geb, City, Major,
                                     POS, Desc, LookingFor, Pic, Experience,
                                     Git)
    except EX.DuplicateAccountException:
        raise EX.DuplicateAccountException


@db_session()
def create_company_account(mail, Psw_Hash, name="-", Appliances=False,
                           Contactname="-", Contactmail="-",
                           SDesc="-", LookingFor="-", Pic=""):
    try:
        DMF.make_new_company_account(mail, Psw_Hash, name, Appliances,
                                     Contactname, Contactmail, SDesc,
                                     LookingFor, Pic)
    except EX.DuplicateAccountException:
        raise EX.DuplicateAccountException


@db_session()
def create_company_contactperson(mail, name, company_mail):
    DMF.make_new_contact_person(mail, name, company_mail)


@db_session()
def update_student_name(mail, name):
    DMF.update_student_name(mail, name)


@db_session()
def update_company_name(mail, name):
    DMF.update_company_name(mail, name)


@db_session()
def update_student_description(mail, SDesc):
    DMF.update_description('student', mail, SDesc)


@db_session()
def update_company_description(mail, SDesc):
    DMF.update_description('company', mail, SDesc)


@db_session()
def update_student_picture(mail, Picturepath):
    DMF.update_picture('student', mail, Picturepath)


@db_session()
def update_company_picture(mail, Picturepath):
    DMF.update_picture('company', mail, Picturepath)


@db_session()
def update_student_lookingfor(mail, LF):
    DMF.update_lookingfor('student', mail, LF)


@db_session()
def update_company_lookingfor(mail, LF):
    DMF.update_lookingfor('company', mail, LF)


@db_session()
def update_student_posname(mail, pos):
    DMF.update_posname(mail, pos)


@db_session()
def update_student_city(mail, City):
    DMF.update_city(mail, City)


@db_session()
def update_student_major(mail, Major):
    DMF.update_major(mail, Major)


@db_session()
def update_student_experience(mail, Experience):
    DMF.update_experience(mail, Experience)


@db_session()
def update_student_github(mail, Git):
    DMF.update_github(mail, Git)


@db_session()
def update_student_hlanguage(mail, data):
    DMF.update_human_languages(mail, data)


@db_session()
def update_student_planguage(mail, data):
    DMF.update_programming_languages(mail, data)


@db_session()
def update_student_oskills(mail, data):
    DMF.update_other_skills(mail, data)


@db_session()
def update_user_password(mail, New_Password):
    if New_Password == "":
        raise EX.PasswordCreationError


@db_session()
def get_student_account(mail):
    Acc = DMF.get_student_account(mail)
    return Acc


@db_session()
def get_company_account(mail):
    Acc = DMF.get_company_account(mail)
    return Acc


@db_session()
def get_student_hlanguage(mail):
    Acc = DMF.get_student_hlanguages(mail)
    return Acc.languages

@db_session()
def get_student_planguage(mail):
    Acc = DMF.get_student_planguages(mail)
    return Acc.languages


@db_session()
def get_student_otherskill(mail):
    Acc = DMF.get_student_oskills(mail)
    return Acc.skills


""" Authentification-Functions """


@db_session()
def set_user_active(mail):
    DMF.set_user_active(mail)
    return True


@db_session()
def set_user_inactive(mail):
    DMF.set_user_inactive(mail)
    return True


@db_session()
def set_user_authentificated(mail):
    DMF.set_user_authentificated(mail)
    return True


@db_session()
def set_user_unauthentificated(mail):
    DMF.set_user_unauthentificated(mail)
    return True


@db_session()
def set_user_anonymous(mail):
    DMF.set_user_anonymous(mail)
    return True


@db_session()
def set_user_unanonymous(mail):
    DMF.set_user_unanonymous(mail)
    return True


@db_session()
def get_user_active(mail):
    Acc = DMF.get_user_active(mail)
    return Acc


@db_session()
def get_user_authentificated(mail):
    Selection = DMF.get_user_authentificated(mail)
    return Selection


@db_session()
def get_user_anonymous(mail):
    Selection = DMF.get_user_anonymous(mail)
    return Selection


@db_session()
def delete_student_account(mail):
    DMF.delete_student_account(mail)


@db_session()
def delete_company_account(mail):
    DMF.delete_company_account(mail)


""" Student Object Getter """


@db_session()
def get_student_name(mail):
    Selection = DMF.get_student_account(mail)
    return Selection.fullname


@db_session()
def get_student_birthdate(mail):
    Selection = DMF.get_student_account(mail)
    return Selection.birthdate


@db_session()
def get_student_city(mail):
    Selection = DMF.get_student_account(mail)
    return Selection.city


@db_session()
def get_student_posname(mail):
    Selection = DMF.get_student_account(mail)
    return Selection.university


@db_session()
def get_student_major(mail):
    Selection = DMF.get_student_account(mail)
    return Selection.major


@db_session()
def get_student_description(mail):
    Selection = DMF.get_student_account(mail)
    return Selection.quote


@db_session()
def get_student_lookingfor(mail):
    Selection = DMF.get_student_account(mail)
    return Selection.looking_for


@db_session()
def get_student_picture(mail):
    Selection = DMF.get_student_account(mail)
    if Selection.profile_picture == "":
        return ""
    else:
        picturepath = DMF.get_student_picturepath(mail)
        return picturepath


@db_session()
def get_student_experience(mail):
    Selection = DMF.get_student_account(mail)
    return Selection.experience


@db_session()
def get_student_git(mail):
    Selection = DMF.get_student_account(mail)
    return Selection.git_account


""" Company Object Getter """


@db_session()
def get_company_name(mail):
    Selection = DMF.get_company_account(mail)
    return Selection.name


@db_session()
def get_company_appliances(mail):
    Selection = DMF.get_company_account(mail)
    return Selection.accept_appliances


@db_session()
def get_company_contactname(mail):
    Selection = DMF.get_company_account(mail)
    return Selection.contact_person


@db_session()
def get_company_contactmail(mail):
    Selection = DMF.get_company_account(mail)
    return Selection.contact_person_mail


@db_session()
def get_company_description(mail):
    Selection = DMF.get_company_account(mail)
    return Selection.description


@db_session()
def get_company_lookingfor(mail):
    Selection = DMF.get_company_account(mail)
    return Selection.looking_for


@db_session()
def get_company_picture(mail):
    Selection = DMF.get_company_account(mail)
    if Selection.profile_picture == "":  # Schutz vor Zugriffen auf None
        return ""
    else:
        picturepath = DMF.get_company_picturepath(mail)
        return picturepath


@db_session()
def search_students(searchlist):
    studentlist = []
    studentlist = DMF.search_all(searchlist)
    return studentlist


if __name__ == 'main':
    pass