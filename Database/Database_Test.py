import json
from Database import Database_Facade as db

import unittest
import datetime

class TestDB(unittest.TestCase):

    def setUp(self):
    
        self.id     = 'matts@peters.org'
        self.hash   = '12345'
        self.name   = 'Matts Peters'
        self.bday   = '1988 04 16'
        self.city   = '<city>'
        self.major  = '<major>'
        self.uni    = '<university>'
        self.quote  = '<quote>'
        self.lf     = '<looking_for>'
        self.pic    = '<pic_url>'
        self.exp    = '<experience>'
        self.git    = '<git>'
                
        db.create_student_account(self.id, self.hash, self.name, self.bday, self.city, self.major, self.uni, self.quote, self.lf, self.pic, self.exp, self.git)
        
        self.h_languages = [
            {
                'language': 'English',
                'level': 'C1'
            }
        ]
        self.p_languages = [
            {
                'language': 'Javascript',
                'level': 'expert'
            },
            {
                'language': 'HTML',
                'level': 'expert'
            },
            {
                'language': 'CSS',
                'level': 'expert'
            }
        ]
        self.o_skills = [
            {
                'skill': 'Scrum Master'
            }
        ]

        db.update_student_hlanguage(self.id, json.dumps(self.h_languages))
        db.update_student_planguage(self.id, json.dumps(self.p_languages))
        db.update_student_oskills(self.id, json.dumps(self.o_skills))

    # GET #

    def test_get_fullname(self):
        self.assertEqual(db.get_student_name(self.id), self.name)

    def test_get_birthdate(self):
        self.assertEqual(db.get_student_birthdate(self.id), datetime.date(1988, 4, 16))

    def test_get_student_city(self):
        self.assertEqual(db.get_student_city(self.id), self.city)

    def test_get_student_major(self):
        self.assertEqual(db.get_student_major(self.id), self.major)

    def test_get_student_university(self):
        self.assertEqual(db.get_student_posname(self.id), self.uni)

    def test_get_student_quote(self):
        self.assertEqual(db.get_student_description(self.id), self.quote)

    def test_get_student_looking_for(self):
        self.assertEqual(db.get_student_lookingfor(self.id), self.lf)

    def test_get_student_picture(self):
        self.assertEqual(db.get_student_picture(self.id), self.pic)

    def test_get_student_experience(self):
        self.assertEqual(db.get_student_experience(self.id), self.exp)

    def test_get_student_git(self):
        self.assertEqual(db.get_student_git(self.id), self.git)

    def test_get_natural_languages(self):
        self.assertEqual(json.loads(db.get_student_hlanguage(self.id)), self.h_languages)
    
    def test_get_programming_languages(self):
        self.assertEqual(json.loads(db.get_student_planguage(self.id)), self.p_languages)

    def test_get_other_skills(self):
        self.assertEqual(json.loads(db.get_student_otherskill(self.id)), self.o_skills)

    # UPDATE #

    def test_update_fullname(self):
       db.update_student_name(self.id, 'M. Peters')
       self.assertEqual(db.get_student_name(self.id), 'M. Peters')

    def test_update_student_city(self):
        db.update_student_city(self.id, 'Manchester')
        self.assertEqual(db.get_student_city(self.id), 'Manchester')

    def test_update_student_major(self):
        db.update_student_major(self.id, 'Computer Science')
        self.assertEqual(db.get_student_major(self.id), 'Computer Science')

    def test_update_student_university(self):
        db.update_student_posname(self.id, 'University of Manchester')
        self.assertEqual(db.get_student_posname(self.id), 'University of Manchester')

    def test_update_student_quote(self):
        db.update_student_description(self.id, '<new_quote>')
        self.assertEqual(db.get_student_description(self.id), '<new_quote>')

    def test_update_student_looking_for(self):
        db.update_student_lookingfor(self.id, '<looking_for2>')
        self.assertEqual(db.get_student_lookingfor(self.id), '<looking_for2>')

    def test_update_student_picture(self):
        db.update_student_picture(self.id, '<new_url>')
        self.assertEqual(db.get_student_picture(self.id), '<new_url>')

    def test_update_student_experience(self):
        db.update_student_experience(self.id, '<new_experience>')
        self.assertEqual(db.get_student_experience(self.id), '<new_experience>')

    def tearDown(self):
        db.delete_student_account(self.id)

if __name__ == '__main__':
    unittest.main()