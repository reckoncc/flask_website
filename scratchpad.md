> need search functionality
    > probably done through iteration of database ("it's a query, sir")
    ? What to search for
    - name
    - city
    - university
    - major
    - h_languages
    - p_languages
    - other_skills
    - experience

    > search needs to happen on `Enter` in Search-Bar

    ? what should the (yet unknown) function return
    ! it should return all the student id's for the student which fulfill the following criteria:
        - substring-match for [name, major, experience, other_skills]

            e. g. name = 'Mattias Petter Johansson',
                    q1 = 'Matt' should return `True`
                    q2 = 'Frederic' should return `False`

        - full-match for [h_languages, p_languages]

            e. g. p_language = 'Javascript'
                    q1 = 'Javascript' should return `True`
                    q2 = 'Java' should return `False`

    ? how should the data be sorted (which first)
    ! theoretically: the best fit first (but: how to define that)
    ! practically: whatever fits in non-specific order
    > we choose the practical approach

    ? how to express search for multiple 'parameters' with search-query
    ! search parameters are separated by white-spaces

    e.g. s = `Stockholm Computer Science Python Javascript Linux Command Line`

    Difficulties:
        - Search-Terms are themselves separated by whitespaces
            Solution: see `Suggested approach`

        - Search should return positive result even if not all parameters (also: search-terms) match
            Solution: say that >= n% must match


    Suggested approach:
        Given string s (here: from above example)
        iterate through each term (e. g. [Stockholm, Computer, Science, Python, Javascript, Linux, Command, Line])

        **if** the term matches any (min. 1) of the fields to be searched (see: what to search for) over (either as a substring or as a full-match, depending)
        **then** count it as a hit

        **if** the hit rate is greater than n% (where n is left for discussion) **then** select the student