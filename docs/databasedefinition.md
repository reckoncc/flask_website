# Database-Definition File

This document is a description of the database-file itself, where entities are described in more detail and their functionality.

In the following code-snippet are the needed imports for everything in the definition of the database:

```Python
from pony.orm import *
from datetime import date
```

In order to make this database work, we need to import everything from pony.orm, as an import of single files would be to large to handle. Also datetime is needed to save birthdates into our database.

As for Entities, there exist the following:
* Student
* Company
* Authentification
* Programming_Language
* Human_Language
* Other_Skills

These were written as Python-Classes. 

<h2>Class Student</h2>
<h4>The Class</h4>

```Python
class Student(db.Entity):
    Student_Mail = PrimaryKey(str)
    Student_Name = Required(str)
    Student_Birthdate = Required(date)
    Student_City = Required(str)
    Student_POSName = Optional(str)
    Student_Major = Optional(str)
    Student_SDesc = Optional(str)
    Student_LookingFor = Optional(str)
    Student_ProfilePicture = Optional(str)
    Student_Experience = Optional(str)
    Student_ProjGit = Optional(str)
```

The entity Student holds a Student-Object, which is used by any student, who registers on our website. The student must fill in his/her Email, his/her name, his/her birthdate and his/her city. If these are not filled in correct (except the birthdate), the student can't create an account, for the database throws an exception. 

<h4>Class-Membervariables</h4>

<b>Student_Mail</b>
```Python
Student_Mail = PrimaryKey(str)
```
This attribute represents the primary key of the Student-Table. It saves the Email of the student, which needs to be unique. Another account with the same Email is not allowed. It is saved as a string.

<b>Student_Name</b>
```Python
Student_Name = Required(str)
```
This attribute represents the name of the student, which contains first name and last name as a string. It is required.

<b>Student_Birthdate</b>
```Python
Student_Birthdate = Required(date)
```
This attribute represents the birthdate of the student, which contains a SQL-Date. As parameter it will be converted from a DD.MM.YYYY in a YYYY-MM-DD later. It is required.

<b>Student_City</b>
```Python
Student_City = Required(str)
```
This attribute represents the city, in which the student lives. It is a string, that is also required. 

<b>Student_POSName</b>
```Python
Student_POSName = Optional(str)
```
The name POS stands for Place of Study. The attribute represents the name of the university/college, where the student studies as a string. It is optional.

<b>Student_Major</b>
```Python
Student_Major = Optional(str)
```
This attribute represents the major, which the student studies as a string. It is optional. 

<b>Student_SDesc</b>
```Python
Student_SDesc = Optional(str)
```
The name SDesc stands for Short Description. This attribute represents a short description, which the student can add about himself. It is saved as a string and is optional.

<b>Student_LookingFor</b>
```Python
Student_LookingFor = Optional(str)
```
The attribute represents a string which contains a short info about what the student is looking for (e.g. work, internships, a.s.o.). It is optional.

<b>Student_ProfilePicture</b>
```Python
Student_ProfilePicture = Optional(str)
```
This attribute represents an Imagefile-Path to an uploaded profile picture. It is saved as a string. If it is None, there is no picture to be displayed, else it contains the path to the requested image. 

<b>Student_Experience</b>
```Python
Student_Experience = Optional(str)
```
This attribute represents any experience, that the student wants to insert. It is a string and optional.

<b>Student_ProjGit</b>
```Python
Student_ProjGit = Optional(str)
```
This attribute represents any Project on Github, which the student wants to show as code example.

<h2>Class Company</h2>

<h4>The Class</h4>

```Python
class Company(db.Entity):
    Company_Mail = PrimaryKey(str)
    Company_Name = Required(str)
    Company_Appliances = Required(bool)
    Company_CPMail = Optional(str)
    Company_CPName = Optional(str)
    Company_SDesc = Optional(str)
    Company_LookingFor = Optional(str)
    Company_ProfilePicture = Optional(str)
```
The entity Company holds a Company-Object, which represents a company. The company can look for students. It must at least have a mail, a name and if they accept appliances. It can also give information about persons to contact if someone is interested in contacting the company. They also have a short description, what they are looking for and a profile picture. These are optional. 

<h4>Class-Membervariables</h4>

<b>Company_Mail</b>
```Python
Company_Mail = PrimaryKey(str)
```
This attribute represents the primary key of the table Company. It contains the mail of the company, which must be unique, so other accounts can't use the same mail. It contains a string and is (obviously) required.

<b>Company_Name</b>
```Python
Company_Name = Required(str)
```
This attribute represents the name of the company. It contains a string and is required.

<b>Company_Appliances</b>
```Python
Company_Appliances = Required(bool)
```
THis attribute represents a boolean, which shows if a company accepts appliances or not at the moment. It is required.

<b>Company_CPMail</b>
```Python
Company_CPMail = Optional(str)
```
This attribute represents the mail of a given contact person. It is a string, which is also optional. 

<b>Company_CPName</b>
```Python
Company_CPName = Optional(str)
```
This attribute represents the name of a given contact person. It is a string, which is also optional. 

<b>Company_SDesc</b>
```Python
Company_SDesc = Optional(str)
```
This attribute represents a short description about the company itself. It is a string, which is also optional. 

<b>Company_LookingFor</b>
```Python
Company_LookingFor = Optional(str)
```
This attribute represents a string which shows what the company is looking for (e.g. Trainees, a.s.o.)

<b>Company_ProfilePicture</b>
```Python
Company_ProfilePicture = Optional(str)
```
This attribute represents an ImageFile-Path, which points to an ImageFile, which was uploaded before by the company. It is a string and optional.

<h2>Class Authentification</h2>

<h4>The Class</h4>

```Python
class Authentification(db.Entity):
    Auth_User_Mail = PrimaryKey(str)
    Auth_Password = Required(str)
    Auth_Is_Authentificated = Optional(bool)
    Auth_Is_Active = Optional(bool)
    Auth_Is_Anonymous = Optional(bool)
```
The Authentification-Entity contains an Authentification-Object, which holds information about the hashed password, if an account is authentificated, if an account is active or anonymous. It is created automatically if a new account is created. Every account has only one entry. If an empty password is given, it throws a "PasswordCreationException", which is described later in the document. 

<h4>Class-Membervariables</h4>

<b>Auth_User_Mail</b>
```Python
Auth_User_Mail = PrimaryKey(str)
```
This attribute is the primary key of the Authentification-Table. It contains the mail of the user as key, so it is unique, as a mail can only be used by one account. 

<b>Auth_Password</b>
```Python
Auth_Password = Required(str)
```
This attribute represents the hashed password of a user, which is created at registration. If a user logins again, his inserted password is then hashed and compared to this saved hash. 

<b>Auth_Is_Authentificated</b>
```Python
Auth_Is_Authentificated = Optional(bool)
```
This attribute represents, if an account is authentificated or not. If it is, this value is true, else it is false.

<b>Auth_Is_Active</b>
```Python
Auth_Is_Active = Optional(bool)
```
This attribute represents, if an account is currently active or not. If it is, this value is true, else it is false.

<b>Auth_Is_Anonymous</b>
```Python
Auth_Is_Anonymous = Optional(bool)
```
This attribute represents, if an account is anonymous or not. This value shows if a person is currently not logged in and wants to access a page. If this happens, it gets true and (normally) shows a Forbidden-Page.

<h2>Class Programming_Language</h2>

<h4>The Class</h4>

```Python
class Programming_Language(db.Entity):
    PL_Student_Mail = Required(str)
    PL_Language = Required(Json)
```
The Programming_Language-Entity contains a Programming_Language-Object, which holds information about the Programming-Languages a student can program in. Every account has only one unique Object. 

<h4>Class-Membervariables</h4>

<b>PL_Student_Mail</b>
```Python
PL_Student_Mail = Required(str)
```
This attribute is the primary key of the Programming_Language-Table. It contains the mail of the connected account.

<b>PL_Language</b>
```Python
PL_Language = Required(Json)
```
This attribute represents the programming languages possessed by the given account.

<h2>Class Human_Language</h2>

<h4>The Class</h4>

```Python
class Human_Language(db.Entity):
    HL_Student_Mail = Required(str)
    HL_Language = Required(Json)
```
The entity Human_Language contains a Human_Language-Object, which contains every "Normal" language(e.g. French, English, a.s.o.). Every account has only one unique object.

<b>HL_Student_Mail</b>
```Python
HL_Student_Mail = Required(str)
```
This attribute is the primary key of the Human_Language-Table. It contains the mail of the connected account. 

<b>HL_Language</b>
```Python
HL_Language = Required(Json)
```
This attribute represents all human languages possessed by the given account.

<h2>Class Other_Skill</h2>

<h4>The Class</h4>

```Python
class Other_Skills(db.Entity):
    OS_Student_Mail = Required(str)
    OS_Skill = Required(Json)
```

<h4>Class-Membervariables</h4>

<b>OS_Student_Mail</b>
```Python
OS_Student_Mail = Required(str)
```
This attribute is the primary key of the Other_Skill-Table. It contains the mail of the connected account.

<b>OS_Skill</b>
```Python
OS_Skill = Required(Json)
```
This attribute represents all other skills possessed by the given account.
