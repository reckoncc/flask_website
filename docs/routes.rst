Routes
=======================

Overview
---------------

+-----------------------------+-----------+--------------------------------------------------+
| Route                       | Methods   | Description                                      |
+-----------------------------+-----------+--------------------------------------------------+
| /                           | GET, POST | renders 'landing_page.html'                      |
+-----------------------------+-----------+--------------------------------------------------+
| /signin                     | GET, POST | renders 'signin.html'                            |
+-----------------------------+-----------+--------------------------------------------------+
| /signup                     | GET, POST | renders 'signup.html'                            |
+-----------------------------+-----------+--------------------------------------------------+
| /signup_student             | GET, POST | renders 'signup_student.html'                    |
+-----------------------------+-----------+--------------------------------------------------+
| /user                       | GET       | renders 'student_profile.html'                   |
+-----------------------------+-----------+--------------------------------------------------+
| /user/natural_languages     | POST      | updates natural languages from student in db     |
+-----------------------------+-----------+--------------------------------------------------+
| /user/programming_languages | POST      | updates programming languages from student in db |
+-----------------------------+-----------+--------------------------------------------------+
| /user/other_skills          | POST      | updates other skills from student in db          |
+-----------------------------+-----------+--------------------------------------------------+
| /user/fullname              | POST      | updates fullname from student in db              |
+-----------------------------+-----------+--------------------------------------------------+
|                             | POST      | updates city from student in db                  |
| /user/city                  |           |                                                  |
+-----------------------------+-----------+--------------------------------------------------+
| /user/major                 | POST      | updates major from student in db                 |
+-----------------------------+-----------+--------------------------------------------------+
| /user/university            | POST      | updates university from student in db            |
+-----------------------------+-----------+--------------------------------------------------+
| /user/quote                 | POST      | update quote from student in db                  |
+-----------------------------+-----------+--------------------------------------------------+
| /user/looking_for           | POST      | updates looking for from student in db           |
+-----------------------------+-----------+--------------------------------------------------+
| /user/experience            | POST      | updates experience from student in db            |
+-----------------------------+-----------+--------------------------------------------------+
| /user/search                | GET       | returns results of search from search bar        |
+-----------------------------+-----------+--------------------------------------------------+
| /upload                     | POST      | updates profile picture from student in db       |
+-----------------------------+-----------+--------------------------------------------------+

Query Strings
----------------------

We consider the URL of a user profile: 

`http://<ip>:<port>/user?id=aleph@gmail.com`

With the field ``id`` we specify the user uniquely via his E-Mail. For editing the profile we add the field ``edit=true``.

Here's how we distinguish the editing state from the none-editing state via the query string:

.. code-block:: python

  @application.route('/user', methods=['GET'])
  def student_profile(pass_edit=False):

  user_id       = request.args.get('id')
  edit          = request.args.get('edit')

  if edit == 'true' or edit == 'True':
    pass_edit = True
  elif edit == 'false' or edit == 'False':
    pass_edit = False
  
  # Fetch user data from database #

  return render_template('student_profile.html', 
                          # other parameters #
                          edit=pass_edit);

This gets passed to the Jinja templates; e.g.:

.. code-block:: html
  
  {% if edit %}
    <a
        id="save"
        class="btn btn-primary"
        type="button"
        href="?id={{user_id}}"
    >
        Save
    </a>
    {% else %}
    <a
        id="edit"
        class="btn btn-primary"
        type="button"
        href="?id={{user_id}}&edit=True"
    >
        Edit
    </a>
    {% endif %}


