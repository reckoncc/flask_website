Project Objectives
======================

Our team's goal was to develop a website akin the Xing job portal, but targeted specifically to students in the fields of information technology et al in search of an internship. 

The original project objectives include:

- Landing Page with lead
- Signup Page
- Student Registration
- Company Registration
- Student Profile (editable)
- Company Profile (editable)
- Search for Students and Companies

Further requirements:

- only users can edit their own profile


Completed Objectives
--------------------
- Landing Page with lead
- Signup Page
- Student Registration
- Company Registration
- Student Profile (editable)
- Search for Students

Missing Objectices
------------------

- Company Profile (editable)
- Search for Companies
- only users can edit their own profile

Technologies Used
-----------------

Languages
^^^^^^^^^^^^^^

- Python
- Javascript
- HTML
- CSS

Frameworks
^^^^^^^^^^^^^^^

- Flask
- jQuery
- Bootstrap
- PonyORM
