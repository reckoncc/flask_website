Code Closeup
======================

Setup/Initialization
-----------------------

The application is set-up in ``app/__init__.py``.

.. code-block:: python
  
  from flask import Flask
  from config import Config
  import os
  from flask_bootstrap import Bootstrap

  # Application Setup
  application = Flask(__name__, static_url_path='/static', static_folder="static")
  application.config.from_object(Config)
  Bootstrap(application)

  UPLOAD_FOLDER = os.path.join(os.path.dirname(os.path.realpath(__file__)), "static/upload")
  application.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

  from app import routes

The app is configured using a configuration object:

.. code-block:: python

 class Config(object):
    CSRF_ENABLED = True
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'J5CoE2Aw85wKf6h'

Flask Bootstrap is used for the Boostrap framework. 

To avoid circular import ``routes`` is imported after setup.

Routes
----------------

We use routes to interface the front-end with the back-end. 

Let's say the user wants to update some of his programming languages.

After the user has edited the table, and pressed **save**, the data from the tables *natural languages*, *programming languages* and *other skills* 
is fetched and sent to the appropriate route via ajax.

.. code-block:: javascript

  var url_call;
    switch (obj.id) {
      case 'tbl-nl':
        url_call = natural_languages_url_call
        break;
      case 'tbl-pl':
        url_call = programming_languages_url_call
        break;
      case 'tbl-os':
        url_call = other_skills_url_call
        break;
    }

    $.ajax({
      method: 'POST',
      url: url_call + $.param({ id: user_id }),
      contentType: 'application/json',
      data: JSON.stringify(data)
    });


The back-end receives the data.

.. code-block:: python

  @application.route('/user/programming_languages', methods=['POST'])
  @handle_post
  def update_programming_languages(user_id, data):
    update_db(user_id, data, db.update_student_planguage)

The handle_post method is a self-defined decorator. It makes the code reusable.

.. code-block:: python

  from functools import wraps

  def handle_post(f):
    @wraps(f)
    def wrapper():
        user_id = request.args.get('id')
        data    = request.get_json()
        f(user_id, data)
        return ok_response()

    return wrapper

The ``ok_response`` method responds to the client that everything is okay (in HTTP language)

.. code-block:: python

  def ok_response():
    resp = make_response()
    resp.headers['Content-Type'] = 'application/json'
    return resp

Image Upload
----------------

To change the profile picture, the user clicks *edit*, then clicks on his current profile picture, where a dialog appears. The user clicks *browse* to select his picture. (The picture should be in square format, since it get's distored otherwise.) When s/he pressed upload the picture should be updated.

Front-End Code:

.. code-block:: javascript
  
  $('#upload-file-btn').click(function() {
    var form_data = new FormData($('#upload-file')[0]);

    $.ajax({
      type: 'POST',
      url: '/upload?' + $.param({ id: user_id }),
      data: form_data,
      contentType: false,
      cache: false,
      processData: false,
      success: function(data) {
        $('#avatar_form').modal('hide');
           $img = $('#avatar');
           $img.attr("src", data);
      }
    });
  });

Back-End Code:

.. code-block:: python
  
  from config import allowed_file
  import os
  
  @application.route('/upload', methods=['POST'])
  def upload_file():
    if request.method == 'POST':
        user_id = request.args.get('id')
        file = request.files['file']

        if file and allowed_file(file.filename):
            filename = user_id + "_" + secure_filename(file.filename)

            file.save(os.path.join(application.config['UPLOAD_FOLDER'], filename))

            img_path = "static/upload/" + filename
            db.update_student_picture(user_id, img_path)
            return img_path

The image gets stored in the ``upload`` folder. The image url gets saved in the db. 


Search
-----------------

The user can search for a students' 

- E-Mail
- Name
- City
- University
- Major
- Looking For (what the student is looking for)
- Programming Languages
- Natural Languages
- Other Skills

.. code-block:: python

  @application.route('/user/search', methods=['GET'])
  def search_bar():
    query_string = request.query_string.decode('utf-8')
    raw_findings = db.search_students(query_string)
    findings = []

    for result in raw_findings:
        languages_json = json.loads(db.get_student_planguage(result.mail))
        languages = []
        for dic in languages_json:
            languages.append(dic['language'])

        finding = {
            'id': result.mail,
            'fullname': result.fullname,
            'city': result.city,
            'profile_picture': result.profile_picture,
            'languages': languages
        }
        findings.append(finding)

    return render_template('/search.html',
                           page_title="Search",
                           signin_form=SigninForm(),
                           findings=findings
                           )

The database search function gets called. The results get assembled for use in Python's Jinja templates.

.. code-block:: html

  {% extends 'base.html' %}

  {% block styles %}
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" />
  {% endblock %}


  {% block content %}
  <div class="container">
      <br />
      {% if findings %}
      <ul class="list-unstyled">
      {% for result in findings %}
      <li class="media" style="background-color: #f6f2f9;">
          <a href="/user?id={{ result.id }}">
            <img class="align-self-center mr-3" width="130px" height="auto" src="{{ result.profile_picture }}"/>
          </a>
          <div class="media-body">
              <br />
              <h5 class="mt-0">{{ result.fullname }}</h5>
              <p><i>{{ result.city }}</i></p>
              <p>
              {% for language in result.languages[:5] %}
              <span class="badge badge-secondary">{{ language }}</span>
              {% endfor %}
              {% if result.languages|length > 5 %}
              <span class="badge badge-secondary">etc...</span>
              {% endif %}
              </p>
          </div>
      </li>
      <br />
      {% endfor %}
      </ul>
      {% else %}
      <div class="alert alert-info" role="alert">
          No results found
      </div>
      {% endif %}
  </div>
  {% endblock %}

Templates
----------------

All non-partial templates extend ``base.html``. This itself extends ``bootstrap/base.html``.

Signup
^^^^^^^^^^^^^^^^

The Signup page for students ``signup_student.html`` uses th Flask-WTF forms. It is used in the Jinja template as follows:

.. code-block:: html

  <div class="form-group {% if form.email.errors %} has-error {% endif %}">
    {{ form.email.label(class="col-sm-4") }}
    {{ form.email(placeholder="Email", class="form-control form-control-lg rounded-0") }}
    {{ form.email.errors[0] }}
  </div>



