.. You-are-IT documentation master file, created by
   sphinx-quickstart on Fri Jun 15 13:55:24 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to You-are-IT's documentation!
======================================

Contents:

.. toctree::
   :maxdepth: 1 

   preamble
   structure
   routes
   code_closeup
   databasedefinition
   unittest

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

