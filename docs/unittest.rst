Unit-Tests
=======================

The unit test is located in ``Database/Database_Test.py``. The test covers the Getters and Setters of the database for the student's profile. The ``setup`` method is used to create a student account for testing.


To run the unit test simply execute the file.
