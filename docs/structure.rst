Project Structure
==========================

::

  /
  |-- app/
  |   |-- static/
  |   |   |-- upload/
  |   |-- templates/
  |   |   |-- includes/
  |-- __init__.py
  |-- forms.py
  |-- routes.py
  |-- utils.py
  |-- Database/
  |   |-- Database_Definition.py
  |   |-- Database_MainFunctions.py
  |   |-- Database_Facade.py
  |   |-- maindb.db
  |-- app.py
  |-- config.py

Overview
-----------------

App
^^^^^^^^^^^^^^^^^

The app folder contains the client-/server side code excluding the database. 

The backend consists of:

- ``__init__.py`` for initializing and configuring the Flask application
- ``routes.py`` for handling the routes
- ``forms.py`` for Flask WTF forms

It also contains the folder ``static`` for external JS/CSS documents; ``static`` itself contains the folder ``upload`` which is used for storing the user's profile pictures.

The HTML files are stored in ``templates``. Partial HTML files (these are included in Jinja templates), are stored in ``templates/includes`` and begin with an underscore per convention.

Database
^^^^^^^^^^^^^^^^^^

The Database consists of:

- ``Database_Definition.py`` for defining the database tables
- ``Database_MainFunctions.py`` for the CRUD functions and helpers
- ``Database_Facade.py`` which utilizes the Facade Pattern and thus provides an easier interface
- ``maindb.db`` the sqlite database file

**The entry point is app.py**
